import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import  GenerateQuote  from  './GenerateQuote';
import  DisplayQuote  from  './DisplayQuote';


class App extends Component {
constructor(props) {
  super(props);
  this.state = {
    // we can set up our sampleEmployee as the default
    // to always display an employee
    quote:  {
      value: "",}
  };
}


getQuote() {
  // Employee collection via fetch
  fetch("https://api.chucknorris.io/jokes/random")
    .then(response  =>  response.json())
    .then(data  => {
      // Once the data is collected, we update our state with the new data
      this.setState({
        quote:  data,
      });
  });
}


render() {
  return (
    <div className="App">
      
      <GenerateQuote  selectQuote={() =>  this.getQuote()}  />
      <DisplayQuote  quote={this.state.quote}  />


    </div>
  );
}
}





export default App;
